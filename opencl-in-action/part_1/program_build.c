/* Listing 2.5 - Building a program from multiple source files */

#define NUM_KERNEL_FILES 2
#define KERNEL_FILE_1 "good_kernel.cl"
#define KERNEL_FILE_2 "bad_kernel.cl"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CL_TARGET_OPENCL_VERSION 120
#ifdef MAC
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

int main(int argc, char const *argv[]) {

  /* Host/Device data structures */
  cl_platform_id platform;
  cl_device_id device;
  cl_context context;
  cl_program program;
  cl_int err;

  /* Program/Kernel data structures */
  FILE *program_handle;
  const char *file_name[] = {KERNEL_FILE_1, KERNEL_FILE_2};
  size_t program_size[NUM_KERNEL_FILES];
  char *program_buffer[NUM_KERNEL_FILES];
  const char build_options[] = "-cl-std=CL1.2 -cl-finite-math-only -cl-no-signed-zeros";
  char *program_log;
  size_t log_size;


  /* Access the first installed platform */
  err = clGetPlatformIDs(1, &platform, NULL);
  if (err < 0) {
    perror("Couldn't find any platforms.");
    exit(1);
  }

  /* Access the CPU / GPU device */
  err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
  if (err == CL_DEVICE_NOT_FOUND) {
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &device, NULL);
  }
  if (err < 0) {
    perror("Couldn't find any device.");
    exit(1);
  }

  /* Create a context */
  context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
  if (err < 0) {
    perror("Couldn't create a context.");
    exit(1);
  }

  /* Read each kernel file and store the contents into a buffer */
  for (size_t i = 0; i < NUM_KERNEL_FILES; i++) {
    program_handle = fopen(file_name[i], "r");
    if (program_handle == NULL) {
      perror("Couldn't find the kernel file(s).");
      exit(1);
    }

    /* Find the size of the kernel program */
    fseek(program_handle, 0, SEEK_END);
    program_size[i] = ftell(program_handle);
    rewind(program_handle);

    /* Allocate the program's buffer and store the contents of the file into the buffer */
    program_buffer[i] = (char*) malloc(program_size[i]+1);
    program_buffer[i][program_size[i]] = '\0';
    fread(program_buffer[i], sizeof(char), program_size[i], program_handle);
    fclose(program_handle);
  }


  /* Create a program containing all program contents */
  program = clCreateProgramWithSource(context, NUM_KERNEL_FILES, (const char**) program_buffer, program_size, &err);
  if (err < 0) {
    perror("Couldn't create the program.");
    exit(1);
  }

  /* Build the program */
  err = clBuildProgram(program, 1, &device, build_options, NULL, NULL);
  if (err < 0) {
    // perror("Couldn't build the program.");
    printf("Build error!!!\n");
    /* Find the size of log and print the log */
    /* Set the log size */
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

    /* Allocate the program_log's buffer and store the contents of the log into the buffer */
    program_log = (char*) malloc(log_size+1);
    program_log[log_size] = '\0';
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size+1, program_log, NULL);
    printf("%s\n", program_log);
    free(program_log);
    exit(1);
  }

  /* Deallocate resources */
  for (size_t i = 0; i < NUM_KERNEL_FILES; i++) {
    free(program_buffer[i]);
  }
  clReleaseProgram(program);
  clReleaseContext(context);

  printf("Build successful!!!\n");

  return 0;
}
