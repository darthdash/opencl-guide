**A complete guide about OpenCL and GPU Computing**

This repository contains self-made study notes and codes during my self-training of OpenCL. I have made these notes especially for my self-learning tasks as I am interested in parallel computing and HPC for scientific research work.
I have created links to the original sources such as books, blogs, or videos. Please acknowledge and support the original authors by buying their books, visiting their blogs, or through any other means possible.

### Sources
1. OpenCL In Action - https://www.manning.com/books/opencl-in-action
2. OpenCL by David Gohara - https://www.youtube.com/playlist?list=PLTfYiv7-a3l7mYEdjk35wfY-KQj5yVXO2
